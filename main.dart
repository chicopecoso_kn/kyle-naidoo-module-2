//                ############################
//                ########Project A###########
//                ############################
//######################################################################
//#Write a basic program that stores and then prints the following data:
//#Your name, favorite app, and city#
//######################################################################
import 'dart:io';

void main() {
  print("What is your name: ");
  String? name = stdin.readLineSync();
  print("What is your favourite app: ");
  String? app = stdin.readLineSync();
  print("What city are you from: ");
  String? city = stdin.readLineSync();

  print("Your name is $name");
  print("Your favourite app is $app");
  print("You are from $city");
}
