//                 ############################
//                 ########Project B###########
//                 ############################
//#############################################################################
//#Create an array to store all the winning apps of the MTN Business App of the
//#                                                     Year Awards since 2012
//#a) Sort and print the apps by name;
//#b) Print the winning app of 2017 and the winning app of 2018.;
//#c) the Print total number of apps from the array.
//#############################################################################
import "dart:io";

void main() {
  List arrWinners = [
    "FNB Banking App is the winning app of 2012",
    "SnapScan is the winning app of 2013",
    "LIVE Inspect is the winning app of 2014",
    "WumDrop is the winning app of 2015",
    "Domestly is the winning app of 2016",
    "Shyft is the winning app of 2017",
    "Khula Ecosystem is the winning app of 2018",
    "Naked Insurance is the winning app of 2019",
    "EasyEquities is the winning app of 2020",
    "Ambani Africa is the winning app of 2021"
  ];

  arrWinners.sort();

  for (var i in arrWinners) {
    print(i);
  }
  print(" ");
  print(arrWinners[7]);
  print(arrWinners[4]);

  print("The total amount of apps in the array is ${arrWinners.length}");
}
