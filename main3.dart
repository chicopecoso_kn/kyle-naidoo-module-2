//                ############################
//                ########Project C###########
//                ############################
//######################################################################
//#Create a class and:
//a) then use an object to print the name of the app, sector/category,
//#  developer, and the year it won MTN Business App of the Year Awards.
//b) Create a function inside the class, transform the app name to all capital
//#  letters and then print the output.
//######################################################################
import "dart:io";

void main() {
  var apps = new MTNApps(
      "EasyEquities", "Best Consumer Solution", "Charles Savage", 2020);
  apps.upperCase();
}

class MTNApps {
  String app_name = "";
  String category = "";
  String developer = "";
  int year = 0;

  MTNApps(String app_name, String category, String developer, int year) {
    this.app_name = app_name;
    this.category = category;
    this.developer = developer;
    this.year = year;
  }

  void upperCase() {
    print("<<<<<Winner of MTN App Academy>>>>>");
    print(app_name.toUpperCase());
    print(category.toUpperCase());
    print(developer.toUpperCase());
    print(year);
  }
}
